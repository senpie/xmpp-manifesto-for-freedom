# XMPP Manifesto for Freedom 
###### Version: 0.1, 03/05/2018

XMPP (formerly known as Jabber) is a federated protocol that is used for chatting with other individuals. Just like any other protocol or messaging service, XMPP/Jabber is susceptible to receiving spam and an array of flooded messages.

We, as individual operators and users of such a service, need to value the fundamental right of freedom and avoid giving in to any sort of censorship that may follow.


### Server Policies
A public server is an XMPP server that allows registration to the public via the client or through web-registration and federation to other XMPP services making it possible for others of one service to reach out to users of another service.

The operators of a public server shall agree the following conditions and understand the following points to fight for user freedom(s):

1.  Spam can be mitigated via client-side entirely.
2.  Restriction of the service based on identifiable information (location, IP, etc.) is not to be done.
3.  Logging of IP address (and other materials) should not be done, or at least, for a short amount of time.
4.  Blocking connections from other servers will censor the meaning of communication and is strictly prohibited.

### Schedule
Starting *immediately*, we will be pushing out this manifesto to fight for the freedom of XMPP users and to push back against censorship toward this federated protocol. We will **not** block access to receiving messages from other servers and will instead encourage other operators to follow us and do the same to ensure the freedom of their users and service.

### Committment
Services who have agree'd to this manifesto and signed,
1. XMPP.is

***If you hereby agree to this manifesto and would like for your service to be listed here, please, form a new issue with your service name, link, and a page describing the service.***

---
*This is in response to "The Jabber Spam Fighting Manifesto": https://github.com/ge0rg/jabber-spam-fighting-manifesto ^1*
